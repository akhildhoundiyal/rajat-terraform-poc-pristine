# --- loadbalancing/main.tf ---


# INTERNET FACING LOAD BALANCER

resource "aws_lb" "pristine_poc_lb" {
  name            = "three-tier-loadbalancer"
  security_groups = [var.lb_sg]
  subnets         = var.public_subnets
  idle_timeout    = 400

  depends_on = [
    var.app_asg
  ]
  tags = merge(
    var.additional_tags
  )
}

resource "aws_lb_target_group" "pristine_poc_tg" {
  name     = "three-tier-lb-tg-${substr(uuid(), 0, 3)}"
  port     = var.tg_port
  protocol = var.tg_protocol
  vpc_id   = var.vpc_id

  lifecycle {
    ignore_changes        = [name]
    create_before_destroy = true
  }
  tags = merge(
    var.additional_tags
  )
}

resource "aws_lb_listener" "pristine_poc_lb_listener" {
  load_balancer_arn = aws_lb.pristine_poc_lb.arn
  port              = var.listener_port
  protocol          = var.listener_protocol
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.pristine_poc_tg.arn
  }
  tags = merge(
    var.additional_tags
  )
}