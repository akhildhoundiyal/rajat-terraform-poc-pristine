# data "aws_ssm_parameter" "pristine_poc-ami" {
#   name = var.image_id
# }


# LAUNCH TEMPLATES AND AUTOSCALING GROUPS FOR BASTION

resource "aws_launch_template" "pristine_poc_bastion" {
  name_prefix            = var.name_prefix
  instance_type          = var.instance_type
  image_id               = var.image_id
  vpc_security_group_ids = [var.bastion_sg]
  key_name               = aws_key_pair.tf-key.key_name

  tags = merge(
    var.additional_tags
  )
}

resource "aws_autoscaling_group" "pristine_poc_bastion" {
  name                = var.name_prefix
  vpc_zone_identifier = var.public_subnets
  min_size            = 1
  max_size            = 1
  desired_capacity    = 1

  launch_template {
    id      = aws_launch_template.pristine_poc_bastion.id
    version = "$Latest"
  }
}


# LAUNCH TEMPLATES AND AUTOSCALING GROUPS FOR FRONTEND APP TIER

resource "aws_launch_template" "pristine_poc_app" {
  name_prefix            = var.name_prefix
  instance_type          = var.instance_type
  image_id               = var.image_id
  vpc_security_group_ids = [var.frontend_app_sg]
#   user_data              = filebase64("install_apache.sh")
  key_name               = aws_key_pair.tf-key.key_name

 tags = merge(
    var.additional_tags
  )
}

data "aws_lb_target_group" "pristine_poc_tg" {
  name = var.lb_tg_name
}

resource "aws_autoscaling_group" "pristine_poc_app" {
  name                = var.name_prefix
  vpc_zone_identifier = var.private_subnets
  min_size            = 1
  max_size            = 2
  desired_capacity    = 1

  target_group_arns = [data.aws_lb_target_group.pristine_poc_tg.arn]

  launch_template {
    id      = aws_launch_template.pristine_poc_app.id
    version = "$Latest"
  }
}


# LAUNCH TEMPLATES AND AUTOSCALING GROUPS FOR BACKEND

resource "aws_launch_template" "pristine_poc_backend" {
  name_prefix            = var.name_prefix
  instance_type          = var.instance_type
  image_id               = var.image_id
  vpc_security_group_ids = [var.backend_app_sg]
  key_name               = aws_key_pair.tf-key.key_name
#   user_data              = filebase64("install_node.sh")

 tags = merge(
    var.additional_tags
  )
}

resource "aws_autoscaling_group" "pristine_poc_backend" {
  name                = var.name_prefix
  vpc_zone_identifier = var.private_subnets
  min_size            = 1
  max_size            = 2
  desired_capacity    = 1

  launch_template {
    id      = aws_launch_template.pristine_poc_backend.id
    version = "$Latest"
  }
}

# AUTOSCALING ATTACHMENT FOR APP TIER TO LOADBALANCER

resource "aws_autoscaling_attachment" "asg_attach" {
  autoscaling_group_name = aws_autoscaling_group.pristine_poc_app.id
  lb_target_group_arn    = var.lb_tg
}