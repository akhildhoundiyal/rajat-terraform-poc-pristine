
variable "region" {}
variable "db_name" {
  type = string
}
variable "dbuser" {
  type      = string
  sensitive = true
}
variable "dbpassword" {
  type      = string
  sensitive = true
}
variable "access_ip" {
  type = string
}
variable "availabilityzone" {}
variable "db_instance_class" {}
variable "db_engine_version" {}
variable "db_identifier"{}
variable "instance_type"{}
variable "vpc_cidr"{}
variable "image_id" {}
variable "name_prefix" {}
variable "additional_tags" {
    type = map(string) 
    default = {
    "Terraform"     = "true"
    "Project"       = "Pristine"
    "Organization"  = "Kellton"
    "Name"          = "pristine-poc-by-Rahul Mishra"
    "Environment"   = "POC"
    "Approved_By"   = "Suraj Kumar"
    "Expiration_by" = "Jan/2023"
  }
}
