region = "us-east-1"
db_name    = "POCdb"
dbuser     = "admin"
dbpassword = "password"
access_ip  = "0.0.0.0/0"
availabilityzone     = ["us-east-1a" , "us-east-1b", "us-east-1c"]
db_instance_class    = "db.t2.micro"
db_engine_version    = "8.0"
db_identifier        = "pristine-poc-db"
instance_type = "t2.micro"
vpc_cidr      = "10.0.0.0/16"
image_id      = "ami-0fe472d8a85bc7b0e"
name_prefix   = "pristine_poc_bastion"








