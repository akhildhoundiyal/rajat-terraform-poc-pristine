provider "aws" {
  region     = var.region
}
resource "random_id" "random_id_prefix" {
  byte_length = 2
}



module "Networking" {
  source               = "./modules/Networking"
  vpc_cidr             = var.vpc_cidr
  access_ip = var.access_ip
  public_sn_count      = 4
  private_sn_count     = 2
  db_subnet_group      = true
  availabilityzone     = var.availabilityzone
  azs                  = 2
  additional_tags = var.additional_tags
}

module "S3" {
  source = "./modules/S3"
  additional_tags = var.additional_tags
}

module "Database" {
  source               = "./modules/Database"
  db_storage           = 8
  db_engine_version    = var.db_engine_version
  db_name              = var.db_name
  db_instance_class    = var.db_instance_class
  dbuser               = var.dbuser
  dbpassword           = var.dbpassword
  db_identifier        = var.db_identifier
  skip_db_snapshot     = true
  rds_sg               = module.Networking.rds_sg
  db_subnet_group_name = module.Networking.db_subnet_group_name[0]
  additional_tags      = var.additional_tags
}

module "compute" {
  source = "./modules/compute"
  frontend_app_sg = module.Networking.frontend_app_sg
  backend_app_sg = module.Networking.backend_app_sg
  bastion_sg =  module.Networking.bastion_sg
  public_subnets = module.Networking.public_subnets
  private_subnets = module.Networking.private_subnets
  bastion_instance_count = 1
  instance_type = var.instance_type
  lb_tg = module.loadbalancing.lb_tg
  lb_tg_name = module.loadbalancing.lb_tg_name
  image_id = var.image_id
  name_prefix = var.name_prefix
  additional_tags = var.additional_tags
}

module "loadbalancing" {
  source = "./modules/loadbalancing"
  lb_sg = module.Networking.lb_sg
  public_subnets = module.Networking.public_subnets
  tg_port = 80
  tg_protocol = "HTTP"
  vpc_id = module.Networking.vpc_id
  app_asg = module.compute.app_asg
  listener_port = 80
  listener_protocol = "HTTP"
  azs = 2
  additional_tags = var.additional_tags
}


